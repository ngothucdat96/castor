/*
*  getUrlParams Challenges
*/

console.log(`
/*
*  getUrlParams Challenges
*/
`)

// this can update more rule in future
function isValidPatternSegment(patternSegment:string):boolean {
    if (patternSegment == '') {
        return false
    }
    return patternSegment != ':'
}

function isParamPatternSegment(patternSegment:string):boolean {
    return patternSegment.length > 1 && patternSegment[0] == ':'
}

function getUrlParams(path:string, pattern:string):object {
    if (path === '' || pattern == '') {
        return {}
    }
    let params:any = {}
    let pathSegments = path.split('/')
    let patternSegments = pattern.split('/')
    let minLength = Math.min(pathSegments.length, patternSegments.length)

    for(let i = 0; i < minLength; i++) {
        let pathSegment = pathSegments[i]
        let patternSegment = patternSegments[i]
        if (!isValidPatternSegment(patternSegment)) {
            break;
        }
        if (isParamPatternSegment(patternSegment)) {
            params[patternSegment.slice(1)] = pathSegment
        } else {
            if (pathSegment != patternSegment) {
                break
            }
        }
    }
    return params
}

const pattern = 'staticOne/:paramOne/staticTwo/staticThree/:paramTwo'

// does not match the first static part: staticOne <> staticZero, returns {}
console.log(getUrlParams('staticZero/one', pattern))

// matched the first static and param part, returns {paramOne: 'one'}
console.log(getUrlParams('staticOne/one', pattern))

// matched the first static and param part with extra, returns {paramOne: 'one'}
console.log(getUrlParams('staticOne/one/staticThree/three', pattern))

// matched the first, second and third static + param parts
// returns {paramOne: 'one', paramTwo: 'two'}
console.log(getUrlParams('staticOne/one/staticTwo/staticThree/two', pattern))



/*
*  objectDiff Challenges
*/

console.log(`
/*
*  objectDiff Challenges
*/
`)

// this will compare differs of two object in deep
function objectDiff<T extends object>(source: T, target:T):object {
    if (typeof(source) != 'object') {
        return {}
    }
    let sourceData = source as any
    let targetData = target as any
    let differs:any = {}
    let visited:any = {}
    Object.keys(sourceData).forEach(key => {
        visited[key] = true
        if (typeof(sourceData[key]) == 'object' && typeof(targetData[key]) == 'object') { // in-case both childs are object, compare keys child
            let childDiffers = objectDiff(sourceData[key], targetData[key])
            if (Object.keys(childDiffers).length > 0) {
                differs[key] = {
                    ...childDiffers
                }
            }
        } else {
            if (sourceData[key] !== targetData[key]) {
                differs[key] = {
                    old: sourceData[key],
                    new: targetData[key]
                }
            }
        }
    })
    Object.keys(targetData).forEach(key => {
        if (!visited[key]) {
            differs[key] = {
                old: undefined,
                new: targetData[key]
            }
        }
    })
    return differs
}

type Data = {id: string, name?: string, count: number}

const before: Data = {id: '1', count: 0} 
const after: Data = {id: '1', name: 'khan', count: 1}
// should read {name: {old: undefined, new: 'khan'}, count: {old: 0, new: 1}}
console.log('---Before: ', before, ', after: ', after)
console.log('Compare: ', objectDiff(before, after))

let befores:Array<object> = [
    { id: 1 },
    { name: 'Ngo Dat' },
    { diffValue: 1, name: { firstName: 'Ngo', lastName: 'Dat'}},
    { name: { firstName: 'Ngo', lastName: 'Dat'}},
    { name: { firstName: 'Ngo', lastName: 'Dat'}}
]

let afters:Array<object> = [
    { id: '1' },
    { name: { firstName: 'Ngo', lastName: 'Dat'}},
    { diffValue: 2, name: { firstName: 'Ngo', lastName: 'Dat'}, addmore: 'text'},
    { name: { firstName: 'ngo', lastName: 'dat'}},
    { name: { firstName: 'Ngo', lastName: 'Dat'}}
]

const testing = function() {
    for(let i = 0; i < befores.length; i++) {
        console.log('---Before: ', befores[i], ', after: ', afters[i])
        console.log('Compare: ', objectDiff(befores[i], afters[i]))
    }
}()